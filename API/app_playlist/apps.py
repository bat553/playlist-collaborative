from django.apps import AppConfig


class AppPlaylistConfig(AppConfig):
    name = "app_playlist"
