from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.

class User(AbstractUser):
    username = models.CharField(max_length=120, null=True)
    email = models.EmailField(unique=True)
    access_token = models.CharField(max_length=120, null=True, unique=True)
    refresh_token = models.CharField(max_length=120, null=True, unique=True)
    REQUIRED_FIELDS = []
    USERNAME_FIELD = "email"
